﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public float speed, maxVelocity;

    private Rigidbody2D playerBody;
    private Animator anim;


    void Awake()
    {
        playerBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        PlayerMoveKeyboard();
		
	}

    void PlayerMoveKeyboard()
    {
        float vel = Mathf.Abs(playerBody.velocity.x);
        float forceX = 0f;
        float horizontalAxis = Input.GetAxisRaw("Horizontal");

        if(horizontalAxis > 0)
        {
            if (CheckVelocity(vel)) forceX = speed;

            Vector3 temp = transform.localScale;
            temp.x = 1.3f;
            SetFaceSide(temp);

            PlayerWalk();

        }
        else if(horizontalAxis < 0)
        {
            if (CheckVelocity(vel)) forceX = -speed;

            Vector3 temp = transform.localScale;
            temp.x = -1.3f;
            SetFaceSide(temp);

            PlayerWalk();
        }
        else
        {
            PlayerStopWalk();
        }

        playerBody.AddForce(new Vector2(forceX, 0));


    }

    private void SetFaceSide(Vector3 temp)
    {
        transform.localScale = temp;
    }

    private void PlayerStopWalk()
    {
        anim.SetBool("Walk", false);
    }

    private bool CheckVelocity(float vel)
    {
        return vel < maxVelocity;
    }

    private void PlayerWalk()
    {
        anim.SetBool("Walk", true);
    }
}
