﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGScaler : MonoBehaviour {

	// Use this for initialization
	void Start () {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();


        float worldHeight = Camera.main.orthographicSize * 2f;
        float worldWidth = worldHeight / Screen.width * Screen.height;

        Vector3 tempScale = transform.localScale;
        float width = spriteRenderer.sprite.bounds.size.x;
        tempScale.x = worldWidth / width;

        transform.localScale = tempScale;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
